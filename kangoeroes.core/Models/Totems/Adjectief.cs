﻿using System;

namespace kangoeroes.core.Models.Totems
{
    public class Adjectief
    {
        public int Id { get; set; }
        
        public string Naam { get; set; }
        
        public DateTime CreatedOn { get; set; }

        public Adjectief()
        {
            CreatedOn = DateTime.Now;
        }
    }
}