﻿using AutoMapper;
using kangoeroes.core.Models;
using kangoeroes.leidingBeheer.Models.ViewModels.Tak;

namespace kangoeroes.leidingBeheer.Models.Profiles
{
  public class TakProfile: Profile
  {
    public TakProfile()
    {
      CreateMap<AddTakViewModel,Tak>();
      CreateMap<UpdateTakViewModel, Tak>();
      CreateMap<Tak, BasicTakViewModel>();

    }
  }
}
