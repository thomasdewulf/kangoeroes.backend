﻿namespace kangoeroes.leidingBeheer.Models.AuthViewModels
{
  public class UserRolesViewModel
  {
    public string RoleName { get; set; }

    public string RoleId { get; set; }

    public bool IsActive { get; set; }
  }
}
