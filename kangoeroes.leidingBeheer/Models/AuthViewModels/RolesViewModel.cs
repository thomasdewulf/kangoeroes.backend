﻿using System.Collections.Generic;

namespace kangoeroes.leidingBeheer.Models.AuthViewModels
{
  public class RolesViewModel
  {
    public IEnumerable<RoleViewModel> Roles
    {
      get;
      set;
    }
  }
}
