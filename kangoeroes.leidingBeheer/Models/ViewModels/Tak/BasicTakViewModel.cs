﻿namespace kangoeroes.leidingBeheer.Models.ViewModels.Tak
{
  public class BasicTakViewModel
  {
    public int Id { get; set; }

    public string Naam { get; set; }

    public int Volgorde { get; set; }

    public int LeidingCount { get; set; }
  }
}
