﻿using System;

namespace kangoeroes.leidingBeheer.Models.ViewModels.Adjectief
{
  public class BasicAdjectiefViewModel
  {
    public int Id { get; set; }

    public string Naam { get; set; }

    public DateTime CreatedOn { get; set; }
  }
}
