﻿using System;

namespace kangoeroes.leidingBeheer.Models.ViewModels.Totem
{
  public class BasicTotemViewModel
  {
    public int Id { get; set; }

    public string Naam { get; set; }

    public DateTime CreatedOn { get; set; }

  }
}
