﻿namespace kangoeroes.leidingBeheer.Services.Auth
{
  public class Auth0Config
  {
    public string Domain { get; set; }

    public string NiClientId { get; set; }

    public string NiClientSecret { get; set; }

    public string Audience { get; set; }
  }
}
